package com.xm.mail.po;

import lombok.Data;

import java.io.Serializable;
import java.util.Date;

/**
 * 表三要素,所有表对应实体类基类
 * @author Ivan yu
 * @date 2020/08/15
 */

public abstract class BasePO implements Serializable {
    /**
     * 表主键
     */
    private String id;
    /**
     * 创建时间
     */
    private Date createTime;
    /**
     * 更新时间
     */
    private Date modifyTime;

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public Date getCreateTime() {
        return createTime;
    }

    public void setCreateTime(Date createTime) {
        this.createTime = createTime;
    }

    public Date getModifyTime() {
        return modifyTime;
    }

    public void setModifyTime(Date modifyTime) {
        this.modifyTime = modifyTime;
    }
}
