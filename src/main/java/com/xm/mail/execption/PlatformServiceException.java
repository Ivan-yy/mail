package com.xm.mail.execption;


import com.xm.mail.common.constant.ResultCodeEnum;

/**
 * 业务异常基础类
 * 所有业务异常继承此类
 */
public class PlatformServiceException extends RuntimeException {
    private int errorCode;

    public PlatformServiceException(ResultCodeEnum resultCodeEnum) {
        this(resultCodeEnum.getCode(),resultCodeEnum.getMsg());
    }

    public PlatformServiceException(int code, String msg){
        super(msg);
        this.errorCode = code;
    }

    public PlatformServiceException() {
    }

    public int getErrorCode() {
        return errorCode;
    }
}
