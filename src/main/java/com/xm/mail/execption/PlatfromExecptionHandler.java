package com.xm.mail.execption;


import com.xm.mail.common.PlatformReturn;
import com.xm.mail.common.constant.ResultCodeEnum;
import lombok.extern.slf4j.Slf4j;
import org.springframework.validation.BindException;
import org.springframework.validation.BindingResult;
import org.springframework.validation.ObjectError;
import org.springframework.web.bind.MethodArgumentNotValidException;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestControllerAdvice;
import org.springframework.web.method.HandlerMethod;

import javax.servlet.http.HttpServletRequest;
import java.util.List;

/**
 * 统一异常处理
 */
@Slf4j
@RestControllerAdvice
public class PlatfromExecptionHandler {
    private static int GENERIC_SERVER_ERROR_CODE = 500;
    private static String GENERIC_SERVER_ERROR_MESSAGE = "服务异常";

    @ResponseBody
    @ExceptionHandler(Exception.class)
    public PlatformReturn handle(HttpServletRequest req, HandlerMethod method, Exception ex) {
        if (ex instanceof BindException) {
            BindingResult bindingResult = ((BindException) ex).getBindingResult();
            if (bindingResult.hasErrors() && bindingResult.hasFieldErrors()) {
                List<ObjectError> errorList = bindingResult.getAllErrors();
                StringBuilder messages = new StringBuilder();
                errorList.forEach(error -> {
                    messages.append(error.getDefaultMessage());
                    messages.append("\n");
                });
                log.error("访问 %s ==>%s 参数校验异常", req.getRequestURI(), method.toString(), ex);
                return PlatformReturn.failure(GENERIC_SERVER_ERROR_CODE, messages.toString());
            }
        }
        else if(ex instanceof MethodArgumentNotValidException) {
            log.warn(String.format("访问 %s ==>%s 参数校验异常", req.getRequestURI(), method.toString()), ex);
            MethodArgumentNotValidException exception = (MethodArgumentNotValidException) ex;
            return PlatformReturn.failure(ResultCodeEnum.PARAM_ERROR.getCode(), exception.getBindingResult().getFieldError().getDefaultMessage());
        }
        else if (ex instanceof PlatformServiceException) {
            PlatformServiceException execption = (PlatformServiceException) ex;
            log.warn(String.format("访问 %s ==>%s 业务异常", req.getRequestURI(), method.toString()), ex);
            return PlatformReturn.failure(execption.getErrorCode(), execption.getMessage());
        }

        log.error(String.format("访问 %s ==>%s 系统异常", req.getRequestURI(), method.toString()), ex);
        return PlatformReturn.failure(GENERIC_SERVER_ERROR_CODE, GENERIC_SERVER_ERROR_MESSAGE);
    }
}
