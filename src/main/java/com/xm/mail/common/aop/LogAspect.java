package com.xm.mail.common.aop;

import cn.hutool.json.JSONUtil;
import com.google.common.collect.Maps;
import com.xm.mail.common.utils.HttpUtils;
import com.xm.mail.dto.SysLog;
import lombok.extern.slf4j.Slf4j;
import org.aspectj.lang.ProceedingJoinPoint;
import org.aspectj.lang.Signature;
import org.aspectj.lang.annotation.Around;
import org.aspectj.lang.annotation.Aspect;
import org.aspectj.lang.annotation.Pointcut;
import org.aspectj.lang.reflect.MethodSignature;
import org.springframework.stereotype.Component;
import org.springframework.web.context.request.RequestContextHolder;
import org.springframework.web.context.request.ServletRequestAttributes;

import javax.servlet.http.HttpServletRequest;
import java.lang.reflect.Method;
import java.util.Date;
import java.util.HashMap;

/**
 * @author Ivan yu
 * @date 2020/07/17
 */
@Slf4j
@Aspect
@Component
public class LogAspect {


    @Pointcut("execution(public * com.xm.mail.controller.*.*(..))")
    public void operationLog() {
    }

    @Around("operationLog()")
    public Object around(ProceedingJoinPoint joinPoint) throws Throwable {
        //获取当前请求对象
        ServletRequestAttributes attributes = (ServletRequestAttributes) RequestContextHolder.getRequestAttributes();
        HttpServletRequest request = attributes.getRequest();
        Signature signature = joinPoint.getSignature();
        MethodSignature methodSignature = (MethodSignature) signature;
        Method method = methodSignature.getMethod();
        String methodName = method.getName();
        String className = method.getDeclaringClass().getName();
        HashMap<String, Object> paramMap = Maps.newHashMapWithExpectedSize(10);
        String[] parameters = methodSignature.getParameterNames();
        Object[] args = joinPoint.getArgs();
        for (int i = 0; i < parameters.length; i++) {
            paramMap.put(parameters[i], args[i]);
        }
        String urlStr = request.getRequestURL().toString();

        SysLog sysLog =
                SysLog.builder()
                        .createTime(new Date())
                        .module(className.substring(className.lastIndexOf(".") + 1, className.lastIndexOf("Controller")))
                        .method(methodName)
                        .params(JSONUtil.toJsonStr(paramMap))
                        .ip(HttpUtils.getIpAddress(request))
                        .url(urlStr).build();
        log.info("----------------------------\n log: {}"+ sysLog);
        return joinPoint.proceed();
    }
}
