package com.xm.mail.common.constant;

/**
 * 项目常量
 * @author Ivan yu
 * @date 2020/07/23
 */
public class ProjectConsts {
    /**
     * 未删除（正常）
     */
    public static final Integer NORMAL = 1;
    /**
     * 已删除
     */
    public static final Integer DELETED = 0;

    /**
     * 顺序排序
     */
    public static final Integer ASC = 0;

    /**
     * 倒叙排序
     */
    public static final Integer DESC = 1;
}
