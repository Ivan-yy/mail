package com.xm.mail.common.constant;

/**
 * @author Ivan yu
 * @date 2020/07/14
 */
public enum ResultCodeEnum {
    /**
     * 成功
     */
    OK(0, "success"),
    /**
     * 公共业务异常
     */
    SERVICE_ERROR(500, "业务异常"),
    /**
     * 无权限
     */
    UNAUTHORIZED(401, "无该权限"),
    /**
     * 未登录或无用户信息
     */
    NOT_LOGIN(402, "未登录或查询不到用户"),
    /**
     *
     */
    PARAM_ERROR(400, "请求参数错误"),

    /**
     * 查询数据异常
     */
    SELECT_ERROR(201, "数据异常"),

    /**
     * 数据库更新异常
     */
    UPDATE_ERROR(202, "更新异常"),
    /**
     * 数据库插入异常
     */
    INSERT_ERROR(203, "插入数据异常"),

    /* 业务异常 code 1开头4位 */

    /**
     * 查询不到该用户
     */
    NONENTITY_USER(1000 + 101, "无该用户"),

    /**
     * 用户已冻结
     */
    HAVE_FROZEN(1000 + 102, "用户已冻结"),

    /**
     * 状态异常
     */
    BAD_STATUS(1000 + 103, "状态异常"),

    /**
     * 登陆失败
     */
    LOGIN_FAIL(1000+104, "账号或密码错误")
    ;
    int code;
    String msg;

    ResultCodeEnum(int code, String msg) {
        this.code = code;
        this.msg = msg;
    }

    public int getCode() {
        return code;
    }

    public String getMsg() {
        return msg;
    }

}
