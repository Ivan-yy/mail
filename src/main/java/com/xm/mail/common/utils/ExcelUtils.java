package com.xm.mail.common.utils;

import cn.hutool.core.collection.CollectionUtil;
import cn.hutool.poi.excel.ExcelUtil;
import cn.hutool.poi.excel.ExcelWriter;
import lombok.extern.slf4j.Slf4j;

import javax.servlet.http.HttpServletResponse;
import java.io.File;
import java.io.IOException;
import java.io.Serializable;
import java.lang.reflect.Field;
import java.util.List;
import java.util.Map;

/**
 * @author Ivan yu
 * @date 2020/07/15
 */
@Slf4j
public class ExcelUtils {

    /**
     * 将数据导出为指定目录下的excel文件
     * @param path 文件目录
     * @param fileName excel文件名
     * @param title 表标题
     * @param data 数据
     */
    public static void excelToPath(String path, String fileName, String title, List<Object> data){
        ExcelWriter writer = null;
        try {
            writer = ExcelUtil.getWriter(path+ File.separator +fileName+".xls");
            int columnNum = getColumnNum(data);
            //表头
            writer.merge(columnNum, title);
            //写数据
            writer.write(data,true);
        } finally {
            if(null != writer){
                writer.close();
            }
        }
    }

    /**
     * excel文件写出到servlet
     * @param response http响应对象
     * @param fileName excel文件名
     * @param title 表标题
     * @param data 数据
     */
    public static void excelToServlet(HttpServletResponse response, String fileName, String title, List<?> data){
        ExcelWriter writer = null;
        try {
            writer = ExcelUtil.getWriter();
            int columnNum = getColumnNum(data);
            //表头
            writer.merge(columnNum, title);
            //写数据
            writer.write(data,true);
            response.setContentType("application/vnd.ms-excel;charset=utf-8");
            response.setHeader("Content-Disposition","attachment;filename="+fileName+".xls");
            writer.flush(response.getOutputStream());
        }catch (IOException e) {
            log.error("[excelToResponse] 获取输出流失败");
        }finally {
            if(null != writer){
                writer.close();
            }
        }
    }

    /**
     * 获取数据列数
     * @param data
     * @return
     */
    private static int getColumnNum(List<?> data){
        if(CollectionUtil.isEmpty(data)){
            return 0;
        }
        int columnNum = 0;
        Object firstRow = data.get(0);
        if( firstRow instanceof Map){
            columnNum = ((Map) firstRow).size();
        }
        else if(firstRow instanceof List){
            columnNum = ((List) firstRow).size();
        }
        else {
            Class<?> rowClass = firstRow.getClass();
            Field[] fields = rowClass.getDeclaredFields();
            columnNum = fields.length;
            if(null != fields){
                for (int i = 0; i < fields.length; i++) {
                    if("serialVersionUID".equals(fields[i].getName())){
                        columnNum -= 1;
                    }
                }
            }

        }
        return columnNum;
    }

}

