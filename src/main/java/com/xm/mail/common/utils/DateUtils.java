package com.xm.mail.common.utils;

import lombok.extern.slf4j.Slf4j;

import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;

/**
 * @author Ivan yu
 * @date 2020/07/15
 */
@Slf4j
public class DateUtils {
    private static final String STANDARD = "yyyy-MM-dd HH:mm:ss";
    private static final ThreadLocal<DateFormat> DATE_FORMAT = ThreadLocal.withInitial(() -> new SimpleDateFormat(STANDARD));

    /**
     * string 转date
     *
     * @param str
     * @return yyyy-MM-dd HH:mm:ss
     */
    public static Date strToDate(String str) {
        Date date = null;
        try {
            date = DATE_FORMAT.get().parse(str);
        } catch (ParseException e) {
            log.error("[strToDate]: 日期转换失败");
        }
        return date;
    }

    /**
     * date转str
     *
     * @param date
     * @return
     */
    public static String dateToStr(Date date) {
        if (null == date) {
            return "";
        }
        String format = DATE_FORMAT.get().format(date);
        return format;
    }


}
