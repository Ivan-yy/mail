/**
 * Copyright (c) 2016-2019 人人开源 All rights reserved.
 *
 * https://www.renren.io
 *
 * 版权所有，侵权必究！
 */

package com.xm.mail.common.utils;

import com.baomidou.mybatisplus.core.metadata.OrderItem;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.google.common.collect.Maps;
import com.xm.mail.common.constant.PageParamConstant;
import com.xm.mail.common.xss.SQLFilter;
import org.apache.commons.lang3.StringUtils;

import java.util.Map;

/**
 * 分页查询参数
 *
 * @author Ivan yu
 */
public class Query<T> {

    /**
     * 根据分页参数构建mybatis-plus分页对象
     * @param currentPage
     * @param pageSize
     * @return
     */
    public Page<T> getPage(Long currentPage, Long pageSize){
        Map<String, Object> params = Maps.newHashMapWithExpectedSize(2);
        params.put(PageParamConstant.PAGE,currentPage);
        params.put(PageParamConstant.LIMIT,pageSize);
        return getPage(params);
    }
    /**
     * 根据params中参数构建mp分页对象
     * @param params see PageParamConstant
     * @return
     */
    public Page<T> getPage(Map<String, Object> params) {
        return this.getPage(params, null, false);
    }

    public Page<T> getPage(Map<String, Object> params, String defaultOrderField, boolean isAsc) {
        //分页参数
        long curPage = 1;
        long limit = 10;

        if(params.get(PageParamConstant.PAGE) != null){
            curPage = (Long) params.get(PageParamConstant.PAGE);
        }
        if(params.get(PageParamConstant.LIMIT) != null){
            limit = (Long) params.get(PageParamConstant.LIMIT);
        }

        //mp分页对象
        Page<T> page = new Page<>(curPage, limit);

        //排序字段
        //防止SQL注入（因为sidx、order是通过拼接SQL实现排序的，会有SQL注入风险）
        String orderField = SQLFilter.sqlInject((String)params.get(PageParamConstant.ORDER_FIELD));
        String order = (String)params.get(PageParamConstant.ORDER);


        //前端字段排序
        if(StringUtils.isNotEmpty(orderField) && StringUtils.isNotEmpty(order)){
            if(PageParamConstant.ASC.equalsIgnoreCase(order)) {
                return  page.addOrder(OrderItem.asc(orderField));
            }else {
                return page.addOrder(OrderItem.desc(orderField));
            }
        }

        //没有排序字段，则不排序
        if(StringUtils.isBlank(defaultOrderField)){
            return page;
        }

        //默认排序
        if(isAsc) {
            page.addOrder(OrderItem.asc(defaultOrderField));
        }else {
            page.addOrder(OrderItem.desc(defaultOrderField));
        }

        return page;
    }
}
