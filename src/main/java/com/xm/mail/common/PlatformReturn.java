package com.xm.mail.common;


import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonInclude.Include;
import com.xm.mail.common.constant.ResultCodeEnum;
import lombok.Data;

import java.io.Serializable;

/**
 * 公共返回对象
 *
 * @author Ivan yu
 * @date 2020/07/14
 */
@Data
public class PlatformReturn<T> implements Serializable {

    private boolean success;


    @JsonInclude(Include.NON_EMPTY)
    private String message = "";

    private int code = 0;

    @JsonInclude(Include.NON_NULL)
    private T results = null;

    public PlatformReturn(boolean success) {
        this.success = success;
    }

    public PlatformReturn(boolean success, int code, String message) {
        this.success = success;
        this.message = message;
        this.code = code;
    }

    public PlatformReturn(boolean success, int code, String message, T results) {
        this.success = success;
        this.code = code;
        this.message = message;
        this.results = results;
    }


    public static PlatformReturn success() {
        return new PlatformReturn(true, ResultCodeEnum.OK.getCode(), ResultCodeEnum.OK.getMsg());
    }

    /**
     * 服务返回成功对象
     * @param results
     * @return
     */
    public static<T> PlatformReturn<T> success(T results) {
        return new PlatformReturn(true, ResultCodeEnum.OK.getCode(), ResultCodeEnum.OK.getMsg(), results);
    }

    /**
     * 服务内异常对象
     * @param resultCodeEnum
     * @return
     */
    public static PlatformReturn failure(ResultCodeEnum resultCodeEnum) {
        return failure(resultCodeEnum.getCode(), resultCodeEnum.getMsg());
    }

    public static PlatformReturn failure(int errorCode, String message) {
        return new PlatformReturn(false, errorCode, message);
    }

    /**
     * 服务内异常需要返回results时调用
     * @param resultCodeEnum
     * @param results
     * @return
     */
    public static <T> PlatformReturn<T> failure(ResultCodeEnum resultCodeEnum, T results){
        return new PlatformReturn(false, resultCodeEnum.getCode(),resultCodeEnum.getMsg(),results);
    }

    public static PlatformReturn notFound(ResultCodeEnum resultCodeEnum) {
        return new PlatformReturn(false, resultCodeEnum.getCode(), resultCodeEnum.getMsg());
    }

    public static PlatformReturn unauthorized() {
        return failure(ResultCodeEnum.UNAUTHORIZED.getCode(), ResultCodeEnum.UNAUTHORIZED.getMsg());
    }


    public boolean isSuccess() {
        return this.success;
    }

    public void setSuccess(boolean success) {
        this.success = success;
    }

    public String getMessage() {
        return this.message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    public int getCode() {
        return this.code;
    }

    public void setCode(int code) {
        this.code = code;
    }



    public T getResults() {
        return this.results;
    }

    public void setResults(T results) {
        this.results = results;
    }
}

