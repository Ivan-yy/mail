package com.xm.mail.dto;

import lombok.Data;

import java.io.Serializable;

/**
 * 分页查询入参实体基类
 * @author Ivan yu
 * @date 2020/07/15
 */
public abstract class BaseReq implements Serializable {
    /**
     * 单页记录个数 默认10
     */
    private Long pageSize = 10L;
    /**
     * 当前页 默认1
     */
    private Long currentPage = 1L;

    public Long getPageSize() {
        return pageSize;
    }

    public void setPageSize(Long pageSize) {
        this.pageSize = pageSize;
    }

    public Long getCurrentPage() {
        return currentPage;
    }

    public void setCurrentPage(Long currentPage) {
        this.currentPage = currentPage;
    }
}
