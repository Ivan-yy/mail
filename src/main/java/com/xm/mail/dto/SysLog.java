package com.xm.mail.dto;

import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableName;
import lombok.Builder;
import lombok.Data;

import java.io.Serializable;
import java.util.Date;

/**
 * @author Ivan yu
 * @date 2020/07/18
 */
@Data
@Builder
@TableName("sys_log")
public class SysLog implements Serializable {

    private static final long serialVersionUID = 3295977133455668085L;
    @TableId(type = IdType.AUTO)
    private Long id;
    /**
     * 操作用户
     */
    private String creator;

    /**
     * 操作时间
     */
    private Date createTime;

    /**
     * 消耗时间
     */
    private Long spendTime;

    /**
     * 操作模块
     */
    private String module;

    /**
     * 操作类型 1:查询 2：新增 3：修改 4：删除
     */
    private Integer type;

    /**
     * 请求类型
     */
    private String method;
    /**
     * 请求入参json str
     */
    private String params;
    /**
     * 请求出参json str
     */
    private String response;
    /**
     * 请求来源IP地址
     */
    private String ip;
    /**
     * 请求url
     */
    private String url;
    /**
     * 操作状态 1:成功 0：失败
     */
    private Integer state;
}
