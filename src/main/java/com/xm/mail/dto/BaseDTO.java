package com.xm.mail.dto;


import java.io.Serializable;

/**
 * @author Ivan yu
 * @date 2020/07/24
 */

public abstract class BaseDTO implements Serializable {
    /**
     * 全局唯一id
     */
    private String globalId;

    public String getOrderNo() {
        return globalId;
    }

    public void setOrderNo(String globalId) {
        this.globalId = globalId;
    }
}
